package jp.gr.java_conf.ktnet.mozeal.ut;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import jp.gr.java_conf.ktnet.mozeal.ExcelLoader;
import jp.gr.java_conf.ktnet.mozeal.ExcelSheetValues;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ExcelLoaderクラスのテストクラスです.
 * @author tanabe
 *
 */
public class ExcelLoaderTest {

  private static final String RESOURCE_DIR = "./src/test/resources/ut/ExcelLoaderTest/";
  
  @Test
  public void loadでエクセルファイルのシート名が取得できること() throws Exception {
    ExcelLoader loader = new ExcelLoader("0.00");
    List<ExcelSheetValues> sheetList = loader.load(new File(RESOURCE_DIR + "sample.xlsx"));
    assertThat(sheetList, hasSize(2));
    
    List<String> sheetNames = sheetList.stream()
                                .map(sheet -> sheet.getName())
                                .collect(Collectors.toList());
    assertThat(sheetNames, hasSize(2));
    assertThat(sheetNames, hasItem("exist1"));
    assertThat(sheetNames, hasItem("exist2"));
  }
  
  @Test
  public void loadでエクセルファイルの行の値が取得できること() throws Exception {
    ExcelLoader loader = new ExcelLoader("0.00");
    List<ExcelSheetValues> sheetList = loader.load(new File(RESOURCE_DIR + "sample.xlsx"));
    
    for (ExcelSheetValues sheet : sheetList) {
      if (sheet.getName().equals("exist1")) {
        assertThat(sheet.getRows(), hasSize(2));
        assertThat(sheet.getRows().get(0), hasEntry("string", "String1-1"));
        assertThat(sheet.getRows().get(0), hasEntry("number", "100.00"));
        assertThat(sheet.getRows().get(1), hasEntry("string", "String1-2"));
        assertThat(sheet.getRows().get(1), hasEntry("number", "-500.00"));
      } else if (sheet.getName().equals("exist2")) {
        assertThat(sheet.getRows(), hasSize(2));
        assertThat(sheet.getRows().get(0), hasEntry("string", "String2-1"));
        assertThat(sheet.getRows().get(0), hasEntry("number", "0.51"));
        assertThat(sheet.getRows().get(1), hasEntry("string", "String2-2"));
        assertThat(sheet.getRows().get(1), hasEntry("number", "-0.99"));
      }
    }
  }
}
