package jp.gr.java_conf.ktnet.mozeal.it;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import jp.gr.java_conf.ktnet.mozeal.Application;
import jp.gr.java_conf.ktnet.mozeal.ApplicationConfig;

import org.junit.Test;

/**
 * アプリケーションの結合テストクラスです.
 * @author tanabe
 *
 */
public class ApplicationTest {

  private static final String RESOURCE_DIR = "./src/test/resources/it/";
  
  @Test
  public void 指定したパラメータ通りに文字列が生成されること() throws Exception {
    ApplicationConfig conf = ApplicationConfig.builder()
        .excelFilePath(RESOURCE_DIR + "excels/parameters.xlsx")
        .templateDirPath(RESOURCE_DIR + "templates")
        .templateFileExtension("vm")
        .templateFileEncode("UTF-8")
        .paramName("rows")
        .numberFormat("0.00")
        .build();
    
    Application app = new Application();
    
    String expected = "exist1 name=String1-1, value=100.00" + System.lineSeparator()
                    + "exist1 name=String1-2, value=-500.00" + System.lineSeparator()
                    + "exist2 name=String2-1, value=0.51" + System.lineSeparator()
                    + "exist2 name=String2-2, value=-0.99" + System.lineSeparator();
    
    assertThat(app.execute(conf), is(expected));
  }
  
  @Test
  public void 指定したパラメータファイル通りに文字列が生成されること() throws Exception {
    String[] args = {"-ini", RESOURCE_DIR + "ini/test1.ini"};
    String expected = "exist1 name=String1-1, value=100.00" + System.lineSeparator()
                    + "exist1 name=String1-2, value=-500.00" + System.lineSeparator()
                    + "exist2 name=String2-1, value=0.51" + System.lineSeparator()
                    + "exist2 name=String2-2, value=-0.99" + System.lineSeparator();
    
    Application app = new Application();
    
    assertThat(app.execute(args), is(expected));
  }
}
