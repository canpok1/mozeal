package jp.gr.java_conf.ktnet.mozeal.ut;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import jp.gr.java_conf.ktnet.mozeal.TextMaker;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * TestMakerクラスのテストクラスです.
 * @author tanabe
 *
 */
public class TextMakerTest {

  private static final String RESOURCE_DIR = "./src/test/resources/ut/TextMakerTest/";
  
  @Test
  public void makeの呼び出しで有効値を渡すと想定の文字列が返されること() {
    
    List<LinkedHashMap<String, Object>> values = new ArrayList<>();
    
    values.add(new LinkedHashMap<>());
    values.get(0).put("name", "name1");
    values.get(0).put("value", "value1");
    
    values.add(new LinkedHashMap<>());
    values.get(1).put("name", "name2");
    values.get(1).put("value", "value2");
    
    TextMaker maker = new TextMaker();
    
    String expected = "name=name1, value=value1" + System.lineSeparator()
                    + "name=name2, value=value2" + System.lineSeparator();
    
    assertThat(maker.make(RESOURCE_DIR + "template.vm", "UTF-8", "rows", values), is(expected));
  }
    
}
