package jp.gr.java_conf.ktnet.mozeal.ut;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import jp.gr.java_conf.ktnet.mozeal.ArgumentAnalyzer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.val;
import org.junit.Rule;
import org.junit.experimental.runners.Enclosed;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

@RunWith(Enclosed.class)
public class ArgumentAnalyzerTest {

  @RunWith(Theories.class)
  public static class コンストラクタに不正なオプションを渡す場合 {
    
    @DataPoints
    public static Fixture[] fixtures = {
        new Fixture(null, NullPointerException.class),  // nullを渡す
    };
    
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    
    @Theory
    public void 例外が発生すること(Fixture fixture) throws Exception {
      expectedException.expect(fixture.getException());
      new ArgumentAnalyzer(fixture.args);
    }
    
    @Data
    @AllArgsConstructor
    public static class Fixture {
      private String[] args;  // コンストラクタに渡す値
      private Class<? extends Exception> exception; // 発生するはずの例外
    }
    
  }  
  
  @RunWith(Theories.class)
  public static class コンストラクタに有効なオプションを渡す場合 {
    
    @DataPoints
    public static Fixture[] fixtures = {
        new Fixture(new String[0], null),  // オプション指定無しならデフォルト値
        new Fixture(new String[]{"-ini", "aaaa"}, "aaaa"),  // iniオプションの指定あり
    };
    
    @Theory
    public void 渡した値が各プロパティにセットされること(Fixture fixture) throws Exception {
      val sut = new ArgumentAnalyzer(fixture.args);
      assertThat(sut.getIniPath(), is(fixture.getIniPath()));
    }
    
    @Data
    @AllArgsConstructor
    public static class Fixture {
      private String[] args;  // コンストラクタに渡す値
      private String iniPath; // iniPathの期待値
    }
    
  }
}
