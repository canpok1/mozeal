package jp.gr.java_conf.ktnet.mozeal.ut;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import jp.gr.java_conf.ktnet.mozeal.ApplicationConfig;
import org.junit.Test;

import java.io.FileNotFoundException;

public class ApplicationConfigTest {

  private static final String RESOURCE_DIR = "./src/test/resources/ut/ApplicationConfigTest/";

  @Test
  public void loadで設定値が空ではないファイルから読みこめること() throws Exception {
    ApplicationConfig config = ApplicationConfig.load(RESOURCE_DIR + "/not_empty.ini");
    
    assertThat(config.getExcelFilePath(), is("value_1"));
    assertThat(config.getTemplateDirPath(), is("value_2"));
    assertThat(config.getTemplateFileExtension(), is("value_3"));
    assertThat(config.getTemplateFileEncode(), is("value_4"));
    assertThat(config.getParamName(), is("value_5"));
    assertThat(config.getNumberFormat(), is("value_6"));
    assertThat(config.getDatetimeFormat(), is("value_7"));
  }
  
  @Test(expected = NullPointerException.class)
  public void loadにnullを渡すと例外発生() throws Exception {
    ApplicationConfig.load(null);
  }
  
  @Test(expected = FileNotFoundException.class)
  public void loadに存在しないファイルパスを渡すと例外発生() throws Exception {
    ApplicationConfig.load(RESOURCE_DIR + "/not_found.ini");
  }
  
  @Test
  public void loadで設定値が空のファイルから読みこめること() throws Exception {
    ApplicationConfig config = ApplicationConfig.load(RESOURCE_DIR + "/empty.ini");
    
    assertThat(config.getExcelFilePath(), is(nullValue()));
    assertThat(config.getTemplateDirPath(), is(nullValue()));
    assertThat(config.getTemplateFileExtension(), is(nullValue()));
    assertThat(config.getTemplateFileEncode(), is(nullValue()));
    assertThat(config.getParamName(), is(nullValue()));
    assertThat(config.getNumberFormat(), is(nullValue()));
    assertThat(config.getDatetimeFormat(), is(nullValue()));
  }
}
