package jp.gr.java_conf.ktnet.mozeal;

import lombok.Getter;
import lombok.NonNull;

/**
 * 実行時引数を解析するクラスです.
 * @author tanabe
 *
 */
@Getter
public class ArgumentAnalyzer {

  private static final String INI_PATH_OPTION = "-ini";
  
  private String iniPath;
  
  /**
   * コンストラクタ.
   * @param args 実行時引数.
   */
  public ArgumentAnalyzer(@NonNull String[] args) {
    iniPath = fetchValue(args, INI_PATH_OPTION, null);
  }

  /**
   * 使い方の説明を取得します.
   * @return 説明.
   */
  public String getManual() {
    return "使い方" + System.lineSeparator()
        + "テンプレートファイルを元に文字列を組み立てます。" + System.lineSeparator()
        + "オプション" + System.lineSeparator()
       + INI_PATH_OPTION + " [パス] : 設定ファイルのパス";
  }
  
  /**
   * 引数から適切な値を取得します.
   * @param args 引数.
   * @param option オプション文字列.
   * @param defValue 規定値.
   * @return 対象の引数.対象が取得できない場合は規定値.
   */
  private static String fetchValue(
      @NonNull String[] args,
      @NonNull String option,
               String defValue) {
    for (int index = 0; index < args.length - 1; index++) {
      if (args[index].equals(option)) {
        return args[index + 1];
      }
    }
    return defValue;
  }
}
