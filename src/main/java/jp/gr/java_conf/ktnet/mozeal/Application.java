package jp.gr.java_conf.ktnet.mozeal;

import lombok.val;

import java.io.File;
import java.util.List;

/**
 * プログラムのエントリポイントを定義するクラス.
 * @author tanabe
 *
 */
public class Application {

  /**
    * プログラムのエントリポイントです.
    * @param args 実行時引数.
    */
  public static void main(String[] args) {
    try {
      Application app = new Application();
      System.out.println(app.execute(args));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  
  /**
   * デフォルト設定を取得します.
   * @return デフォルト値を格納したビルダーオブジェクト.
   */
  private static ApplicationConfig.ApplicationConfigBuilder makeDefaultConf() {
    return ApplicationConfig.builder()
            .excelFilePath("./templates/parameters.xlsx")
            .templateDirPath("./templates")
            .templateFileExtension("vm")
            .templateFileEncode("UTF-8")
            .paramName("rows")
            .numberFormat("0.00");
  }
  
  /**
   * 指定設定にしたがって処理を行います.
   * @param args コマンドライン引数.
   * @throws Exception 例外発生.
   */
  public String execute(String[] args) throws Exception {
    val analyzer = new ArgumentAnalyzer(args);
    ApplicationConfig config;
    if (analyzer.getIniPath() != null) {
      config = ApplicationConfig.load(analyzer.getIniPath());
    } else {
      config = makeDefaultConf().build();
    }
    return this.execute(config);
  }
  
  /**
   * 指定設定にしたがって処理を行います.
   * @param config 設定.
   * @throws Exception 例外発生.
   */
  public String execute(ApplicationConfig config) throws Exception {
    
    ExcelLoader loader = new ExcelLoader(config.getNumberFormat());

    List<ExcelSheetValues> sheets = loader.load(new File(config.getExcelFilePath()));
    
    StringBuilder result = new StringBuilder();
    sheets.stream().forEach(
        sheet -> {
          String templateFileName = sheet.getName() + "." + config.getTemplateFileExtension();
          File templateFile = new File(config.getTemplateDirPath(), templateFileName);
          TextMaker maker = new TextMaker();
          result.append(
              maker.make(
                  templateFile.getPath(),
                  config.getTemplateFileEncode(),
                  config.getParamName(),
                  sheet.getRows()
                )
            );
        }
    );
    
    return result.toString();
  }
  
}
