package jp.gr.java_conf.ktnet.mozeal;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * エクセルファイルを読み込むクラスです.
 * @author tanabe
 *
 */
public class ExcelLoader {
  
  /**
   * 数値をフォーマットするオブジェクト.
   */
  private final DecimalFormat decimalFormat;

  /**
   * コンストラクタ.
   * @param numberFormat 数値の形式.
   */
  public ExcelLoader(String numberFormat) {
    decimalFormat = new DecimalFormat(numberFormat);
  }
  
  /**
   * エクセルファイルから値を読み込みます.
   * @param excelFile エクセルファイル.
   * @return 読み込んだ値.
   * @throws IOException 入出力エラー.
   * @throws InvalidFormatException フォーマット違反.
   * @throws EncryptedDocumentException 暗号化されたドキュメント.
   */
  public List<ExcelSheetValues> load(File excelFile)
      throws EncryptedDocumentException, InvalidFormatException, IOException {
    Workbook workbook = WorkbookFactory.create(excelFile);
    
    int firstSheetIndex = 0;
    int lastSheetIndex = workbook.getNumberOfSheets() - 1;
    List<ExcelSheetValues> list = new ArrayList<>();
    for (int sheetIndex = firstSheetIndex; sheetIndex <= lastSheetIndex; sheetIndex++) {
      // 1シートずつ読み込む
      Sheet sheet = workbook.getSheetAt(sheetIndex);
      if (sheet.getLastRowNum() == 0) {
        // 空っぽシートはスキップ
        continue;
      }
      
      // シート名と各行の値を読み込んでオブジェクト詰め込む
      ExcelSheetValues sheetValues = new ExcelSheetValues(sheet.getSheetName());
      
      Row labelRow = sheet.getRow(0);
      int firstRowIndex = 1;
      int lastRowIndex = sheet.getLastRowNum();
      
      for (int rowIndex = firstRowIndex; rowIndex <= lastRowIndex; rowIndex++) {
        // 1行ずつ読み込む
        LinkedHashMap<String, Object> loadedRowValues = new LinkedHashMap<>();
        Row loadTargetRow = sheet.getRow(rowIndex);
        int firstColNum = labelRow.getFirstCellNum();
        int lastColNum = labelRow.getLastCellNum();
        for (int colIndex = firstColNum; colIndex <= lastColNum; colIndex++) {
          
          Cell labelCell = labelRow.getCell(colIndex);
          if (labelCell == null) {
            continue;
          }
          String colName = labelCell.getStringCellValue();
          
          Cell loadTargetCell = loadTargetRow.getCell(colIndex);
          // 例外発生を防ぐため、セルの値の種類に応じて取得メソッドを切り替える
          switch (loadTargetCell.getCellType()) {
            case Cell.CELL_TYPE_STRING :
              loadedRowValues.put(colName, loadTargetCell.getStringCellValue());
              break;
            case Cell.CELL_TYPE_NUMERIC :
              String value = decimalFormat.format(loadTargetCell.getNumericCellValue());
              loadedRowValues.put(colName, value);
              break;
            case Cell.CELL_TYPE_BOOLEAN :
              loadedRowValues.put(colName, loadTargetCell.getBooleanCellValue());
              break;
            default :
              loadedRowValues.put(colName, "");
          }
        }
        sheetValues.getRows().add(loadedRowValues);
      }
      
      list.add(sheetValues);
    }
      
    return list;
  }
  
}
