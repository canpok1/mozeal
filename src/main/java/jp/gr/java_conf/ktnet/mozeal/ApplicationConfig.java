package jp.gr.java_conf.ktnet.mozeal;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * アプリの動作設定を保持するクラスです.
 * @author tanabe
 *
 */
@Data
@Builder
public class ApplicationConfig {

  /**
   * パラメータを記入したエクセルファイルのパス.
   */
  private String excelFilePath;
  
  /**
   * テンプレートファイルのディレクトリパス.
   */
  private String templateDirPath;
  
  /**
   * テンプレートファイルの拡張子.
   */
  private String templateFileExtension;
  
  /**
   * テンプレートファイルの文字コード.
   */
  private String templateFileEncode;
  
  /**
   * テンプレートファイル内で指定するパラメータ名.
   */
  private String paramName;
  
  /**
   * 数値をマージするときのフォーマット.
   */
  private String numberFormat;
  
  /**
   * 日時をマージするときのフォーマット.
   */
  private String datetimeFormat;
  
  /**
   * ファイルから設定を読み込みます.
   * @param filePath ファイルパス.
   * @return 読み込んだ設定値.
   * @throws IOException 入出力エラー.
   * @throws FileNotFoundException ファイルが見つからない.
   */
  public static ApplicationConfig load(@NonNull String filePath)
      throws FileNotFoundException, IOException {
    Properties prop = new Properties();
    try (FileInputStream stream = new FileInputStream(filePath)) {
      prop.load(stream);
    }
    
    return ApplicationConfig
      .builder()
      .excelFilePath(prop.getProperty("excelFilePath"))
      .templateDirPath(prop.getProperty("templateDirPath"))
      .templateFileExtension(prop.getProperty("templateFileExtension"))
      .templateFileEncode(prop.getProperty("templateFileEncode"))
      .paramName(prop.getProperty("paramName"))
      .numberFormat(prop.getProperty("numberFormat"))
      .datetimeFormat(prop.getProperty("datetimeFormat"))
      .build();
  }
}
