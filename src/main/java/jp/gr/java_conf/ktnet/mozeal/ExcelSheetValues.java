package jp.gr.java_conf.ktnet.mozeal;

import lombok.Data;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * エクセルシートの値を保持するクラスです.
 * @author tanabe
 *
 */
@Data
public class ExcelSheetValues {
  
  /**
   * シート名.
   */
  private final String name;
  
  /**
   * 値.
   */
  private final List<LinkedHashMap<String, Object>> rows;
  
  /**
   * コンストラクタ.
   * @param name シート名.
   */
  public ExcelSheetValues(String name) {
    this.name = name;
    this.rows = new ArrayList<>();
  }
}
