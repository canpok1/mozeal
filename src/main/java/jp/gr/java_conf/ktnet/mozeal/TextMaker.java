package jp.gr.java_conf.ktnet.mozeal;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * テンプレートを元に文字列を組み立てるクラスです.
 * @author tanabe
 *
 */
public class TextMaker {

  /**
   * 文字列を組み立てます.
   * @param templateFilePath テンプレートファイルのパス.
   * @param templateEncode テンプレートファイルの文字コード.
   * @param name テンプレファイル内における変数名.
   * @param rows 変数の値.
   * @return 組み立てた文字列.
   */
  public String make(
      String templateFilePath,
      String templateEncode,
      String name,
      List<LinkedHashMap<String, Object>> rows) {
    Velocity.init();
    Template template = Velocity.getTemplate(templateFilePath, templateEncode);
    
    VelocityContext context = new VelocityContext();
    context.put(name, rows);
    
    StringWriter sw = new StringWriter();
    template.merge(context, sw);
    
    return sw.toString();
  }
}
