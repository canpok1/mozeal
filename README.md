# Mozeal

テンプレートファイルを元に文字列を組み立てるツールです。

## Description

テンプレートファイルを元に文字列を組み立てます。

パラメータはエクセルファイルから読み込みます。


## Features

- 文字列を組み立てる


## Requirement

- Java8以上

## Usage

1. テンプレートファイルを準備。

2. エクセルファイルを準備。

    * シート名はテンプレートファイル名と同じにすること。
    * シートの1行目は列名。テンプレートファイルで使用される名前になります。
    * シートの2行目以降はパラメータ。

3. 設定ファイルを準備。

4. 実行時引数に`-ini 設定ファイルのパス`を指定して実行。

## License

[MIT](http://b4b4r07.mit-license.org)
